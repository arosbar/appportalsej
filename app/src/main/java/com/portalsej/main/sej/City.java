package com.portalsej.main.sej;

/**
 * Created by manuel on 27/11/15.
 */
public class City {

    private int id;
    private String titulo;
    private String body;
    private String fecha;
    private String categoria;
    private String file;
    private String nid;
    private String url;

    public City() {
    }

    public City(String titulo, String body, String fecha,String categoria, String file,String nid,String url) {
        this.titulo = titulo;
        this.body = body;
        this.fecha = fecha;
        this.categoria = categoria;
        this.file = file;
        this.nid = nid;
        this.url = url;
    }

    public City(int id, String titulo, String body, String fecha, String categoria, String file, String nid,String url) {
        this.id = id;
        this.titulo = titulo;
        this.body = body;
        this.fecha = fecha;
        this.categoria = categoria;
        this.file = file;
        this.nid = nid;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCategoria(){
        return categoria;
    }

    public void setCategoria(String categoria){
        this.categoria = categoria;
    }

    public String getFile(){
        return file;
    }

    public void setFile(String file){
        this.file = file;
    }

    public String getNid(){
        return nid;
    }

    public void setNid(String nid){
        this.nid = nid;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }


}
