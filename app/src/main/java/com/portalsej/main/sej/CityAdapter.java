package com.portalsej.main.sej;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by manuel on 27/11/15.
 */

public class CityAdapter extends BaseAdapter {

    Context context;
    ArrayList<City> listData;

    public CityAdapter(Context context,ArrayList<City> listData){
        this.context = context;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    class ViewHolder {
        private TextView Title;
        private TextView Body;
        private TextView Date;
        private  TextView Category;

    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.call_item_list,null);
            viewHolder = new ViewHolder();

            viewHolder.Title = (TextView) view.findViewById(R.id.title);
            view.setTag(viewHolder);

            viewHolder.Body = (TextView) view.findViewById(R.id.body);
            view.setTag(viewHolder);

            viewHolder.Date = (TextView) view.findViewById(R.id.date);
            view.setTag(viewHolder);
            viewHolder.Category =  (TextView) view.findViewById(R.id.category);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        City city = listData.get(position);

        String Title = city.getTitulo();
        viewHolder.Title.setText(Title);

        //String Body = city.getBody();
        //viewHolder.Body.setText(Body);

        String Date = city.getFecha();
        viewHolder.Date.setText(Date);

        String Cat = city.getCategoria();
        viewHolder.Category.setText(Cat);


        //viewHolder.Body.setText(Body);
        return view;
    }
}
