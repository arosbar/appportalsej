package com.portalsej.main.sej;

/**
 * Created by manuel on 27/11/15.
 */


import java.util.ArrayList;

/**
 * Created by PP on 6/14/2015.
 */
public interface CityListener {

    public void addCity(City city);

    public ArrayList<City> getAllCity();

    public int getCityCount();
}
