package com.portalsej.main.sej;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by manuel on 27/11/15.
 */

public class DBHandler extends SQLiteOpenHelper implements CityListener{
    private static final int DB_VERSION = 3;
    private static final String DB_NAME = "CallsDatabase.db";
    private static final String TABLE_NAME = "call_table";
    private static final String KEY_ID = "_id";
    private static final String KEY_TITULO = "_titulo";
    private static final String KEY_BODY = "_body";
    private static final String KEY_FECHA = "_fecha";
    private static final String KEY_CATEGORIA = "_categoria";
    private static final String KEY_FILE = "_file";
    private static final String KEY_NID = "_nid";
    private static final String KEY_URL = "_url";

    String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" INTEGER PRIMARY KEY,"+KEY_TITULO+" TEXT,"+KEY_BODY+" TEXT,"+KEY_FECHA+" TEXT,"+KEY_CATEGORIA+" TEXT, "+KEY_FILE+" TEXT, "+KEY_NID+" TEXT, "+KEY_URL+" TEXT)";
    String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
    public void Tuuncate(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from call_table");
    }


    @Override
    public void addCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(KEY_TITULO, city.getTitulo());
            values.put(KEY_BODY, city.getBody());
            values.put(KEY_FECHA,city.getFecha());
            values.put(KEY_CATEGORIA,city.getCategoria());
            values.put(KEY_FILE,city.getFile());
            values.put(KEY_NID,city.getNid());
            values.put(KEY_URL,city.getUrl());

            db.insert(TABLE_NAME, null, values);
            db.close();
        }catch (Exception e){
            Log.e("problem", e + "");
        }
    }
    @Override
    public ArrayList<City> getAllCity() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<City> cityList = null;
        try{
            cityList = new ArrayList<City>();
            String QUERY = "SELECT * FROM "+TABLE_NAME;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    City city = new City();
                    city.setId(cursor.getInt(0));
                    city.setTitulo(cursor.getString(1));
                    city.setBody(cursor.getString(2));
                    city.setFecha(cursor.getString(3));
                    city.setCategoria(cursor.getString(4));
                    city.setFile(cursor.getString(5));
                    city.setNid(cursor.getString(6));
                    city.setUrl(cursor.getString(7));
                    cityList.add(city);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return cityList;
    }

    @Override
    public int getCityCount() {
        int num = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+TABLE_NAME;
            Cursor cursor = db.rawQuery(QUERY, null);
            num = cursor.getCount();
            db.close();
            return num;
        }catch (Exception e){
            Log.e("error",e+"");
        }
        return 0;
    }
}