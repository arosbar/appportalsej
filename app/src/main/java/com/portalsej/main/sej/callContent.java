package com.portalsej.main.sej;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class callContent extends AppCompatActivity {
    TextView Title;
    TextView Body;
    TextView Date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Title = (TextView) findViewById(R.id.title);
        Body = (TextView) findViewById(R.id.body);
        Date = (TextView) findViewById(R.id.date);

        final Bundle data = getIntent().getExtras();

        Title.setText(data.getString("TITLE"));
        Body.setText(data.getString("BODY"));
        Date.setText(data.getString("DATE"));

        Toolbar toolbarCard = (Toolbar) findViewById(R.id.toolbarShare);
        toolbarCard.inflateMenu(R.menu.menu_call_content);
        toolbarCard.setTitle("Descargar archivos");
        toolbarCard.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if (id == R.id.download) {
                            Log.i("Toolbar 2", "Descarga de archivos");

                        }
                        return true;
                    }
                });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.share);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, data.getString("URL"));
                startActivity(Intent.createChooser(intent, "Compartir vía..."));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
