package com.portalsej.main.sej;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;

public class getCalls extends AppCompatActivity {

    ListView listView;
    CityAdapter adapter;
    ArrayList<City> cityArrayList;
    DBHandler handler;

    ProgressBar loader;

    NetworkUtils utils = new NetworkUtils(getCalls.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_calls);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.listview);
        handler = new DBHandler(this);
        loader = (ProgressBar) findViewById(R.id.progressBar);

        loader.setVisibility(View.VISIBLE);
/*
        ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Cargando...");
        new MyTask(progress, this).execute();
*/

        if(utils.isConnectingToInternet())
        {
            if(handler.getCityCount() != 0){
                handler.Tuuncate();
            }
            new DataFetcherTask().execute();
        }
        else
        {
            final ArrayList<City> cityList = handler.getAllCity();
            adapter = new CityAdapter(getCalls.this,cityList);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id){
                    City itemCliked = cityList.get(position);
                    Intent callContent = new Intent(getCalls.this, callContent.class);
                    Bundle data = new Bundle();
                    data.putString("TITLE",itemCliked.getTitulo());
                    data.putString("BODY",itemCliked.getBody());
                    data.putString("DATE",itemCliked.getFecha());
                    data.putString("URL",itemCliked.getUrl());
                    callContent.putExtras(data);
                    startActivity(callContent);
                }
            });
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progress;
        getCalls act;

        public MyTask(ProgressDialog progress, getCalls act) {
            this.progress = progress;
            this.act = act;
        }

        public void onPreExecute() {
            progress.show();
            //aquí se puede colocar código a ejecutarse previo
            //a la operación
        }

        public void onPostExecute(Void unused) {
            //aquí se puede colocar código que
            //se ejecutará tras finalizar
            progress.dismiss();
        }

        protected Void doInBackground(Void... params) {

            //realizar la operación aquí
            return null;
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.get_calls_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.search){
            LayoutInflater toastInflater = getLayoutInflater();
            View toastLayout = toastInflater.inflate(R.layout.toast_layout,(ViewGroup) findViewById(R.id.toast_layout_root));
            TextView toastText = (TextView) toastLayout.findViewById(R.id.text);
            toastText.setText("El Botón de busqueda esta por ahora deshabilitado espera actializaciones");
            Toast toastSearch = new Toast(getApplicationContext());
            toastSearch.setGravity(Gravity.CENTER|Gravity.CENTER,0,0);
            toastSearch.setDuration(Toast.LENGTH_SHORT);
            toastSearch.setView(toastLayout);
            toastSearch.show();
        }
        if(id == R.id.refresh) {
            if(utils.isConnectingToInternet()){
                if(handler.getCityCount() != 0){
                    handler.Tuuncate();
                }
                new DataFetcherTask().execute();
            }
            else{
                Toast.makeText(getCalls.this, "No hay conexión a Internet", Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    class DataFetcherTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverData = null;// String object to store fetched data from server
            // Http Request Code start
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://estudiaen.jalisco.gob.mx/appservices/convocatoria.php");
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                serverData = EntityUtils.toString(httpEntity);
                Log.d("response", serverData);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Http Request Code end
            // Json Parsing Code Start
            try {
                cityArrayList = new ArrayList<City>();
                JSONObject jsonObject = new JSONObject(serverData);
                JSONArray jsonArray = jsonObject.getJSONArray("calls");
                for (int i=0;i<jsonArray.length();i++)
                {
                    JSONObject jsonObjectCity = jsonArray.getJSONObject(i);

                    String cityTitulo = jsonObjectCity.getString("titulo");
                    if(cityTitulo.indexOf("&quot;") > 0 ){
                        cityTitulo = cityTitulo.replaceAll("&quot;", "\"");
                    }

                    String cityBody = jsonObjectCity.getString("cuerpo");

                    String cityFecha = jsonObjectCity.getString("fecha de vigencia");
                    if(cityFecha.indexOf("T00:00:00") > 0){
                        cityFecha = cityFecha.replace("T00:00:00","");
                    }
                    if(cityFecha.indexOf("to") > 0){
                        cityFecha = cityFecha.replace("to","al");
                    }

                    String cityCategoria = jsonObjectCity.getString("nivel");
                    String cityNid = jsonObjectCity.getString("nid");
                    String cityUrl = jsonObjectCity.getString("url");

                    City city = new City();
                    city.setTitulo(cityTitulo);
                    city.setBody(cityBody);
                    city.setFecha(cityFecha);
                    city.setCategoria(cityCategoria);
                    city.setNid(cityNid);
                    city.setUrl(cityUrl);

                    handler.addCity(city);// Inserting into DB
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Json Parsing code end
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.INVISIBLE);
            final ArrayList<City> cityList = handler.getAllCity();
            adapter = new CityAdapter(getCalls.this,cityList);
            listView.setAdapter(adapter);


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id){
                    City itemCliked = cityList.get(position);
                    Intent callContent = new Intent(getCalls.this, callContent.class);
                    Bundle data = new Bundle();
                    data.putString("TITLE",itemCliked.getTitulo());
                    data.putString("BODY",itemCliked.getBody());
                    data.putString("DATE",itemCliked.getFecha());
                    data.putString("URL",itemCliked.getUrl());
                    callContent.putExtras(data);
                    startActivity(callContent);
                }
            });
        }
    }

}
